# -*- coding: utf-8 -*-
import os
import re
import ovh
import yaml
import html
import copy
import datetime
import smtplib

now = datetime.datetime.now(datetime.timezone.utc)
yesterday = now + datetime.timedelta(-1)


class Config(object):
    def __init__(self, filepath):
        """
        Args:
            filepath (str): Path to config file
        """
        if not os.path.isfile(filepath):
            raise (f"Config file '{filepath}' does not exist")
        with open(filepath) as filedata:
            self.config = yaml.safe_load(filedata.read())

        self.ovh_endpoint = self.config['ovh']['endpoint']
        self.app_key = self.config['ovh']['application_key']
        self.app_secret = self.config['ovh']['application_secret']
        self.billing_account = self.config['ovh']['billing_account']
        self.service_name = self.config['ovh']['service_name']
        self.consumer_key = self.config['ovh']['consumer_key']

        self.mail_login = self.config['smtp']['login']
        self.mail_pass = self.config['smtp']['password']
        self.mail_server = self.config['smtp']['server']
        self.mail_port = self.config['smtp']['port']

class ovhcalls(object):
    def __init__(self, config):
        self.config = config
        self.client = ovh.Client(self.config.ovh_endpoint,  self.config.app_key, self.config.app_secret, self.config.consumer_key)
        if now.day == 1:
            self.calls = self.client.get('/telephony/' + self.config.billing_account + '/service/' + self.config.service_name + '/previousVoiceConsumption')
        else:
            self.calls = self.client.get('/telephony/' + self.config.billing_account + '/service/' + self.config.service_name + '/voiceConsumption')

        self.calls_data= {}
        for id in self.calls:
            self.calldata = {}
            self.calldata = self.client.get('/telephony/' + self.config.billing_account + '/service/' + self.config.service_name + '/voiceConsumption/'+str(id))
            self.calls_data[id] = self.calldata



    def summary(self):
        self.calls_sum = {}
        for i in self.calls_data:
            creationDatetime = datetime.datetime.strptime(self.calls_data[i]['creationDatetime'], '%Y-%m-%dT%H:%M:%S%z')
            calling = self.calls_data[i]['calling']
            wayType = self.calls_data[i]['wayType']
            self.calls_sum[i] = {}
            self.calls_sum[i]['creationDatetime'] = creationDatetime
            self.calls_sum[i]['calling'] = calling
            self.calls_sum[i]['wayType'] = wayType
        return self.calls_sum

    def get_yesterday_in(self, calls_data):
        self.yesterday_in = {}
        for i in calls_data:
            creationDatetime = calls_data[i]['creationDatetime']
            if creationDatetime.day == yesterday.day:
                if calls_data[i]['wayType'] != 'outgoing':
                    self.yesterday_in[i] = calls_data[i]

        return self.yesterday_in


def mysort(data):
    data = sorted(data.items(), key = lambda x: x[1]['creationDatetime'])
    return data                   


def translate(data):
    """
    Args:
        calls_data (tulp): output of self.data.sort()
    """

    tr_data = copy.deepcopy(data)
    for i in tr_data:

        #waytype
        for r in (("incoming", "entrant"), ("outgoing", "sortant"), ("transfer", "transfere")):
            wayType = i[1]['wayType'].replace(*r)
            i[1]['wayType'] = wayType
        
        #calling
        i[1]['calling'] = re.sub('00','+',i[1]['calling'])

        #creationdatetime
        creationdatetime = i[1]['creationDatetime']
        fmt = 'Le %d-%m-%Y - %Hh%Mmin%Ss'
        i[1]['creationDatetime'] = creationdatetime.strftime(fmt)

    return tr_data

class send_mail:

    def __init__(self, config, message):
        self.config = config
        self.message = message

        smtpobject = smtplib.SMTP_SSL(self.config.mail_server, self.config.mail_port)
        smtpobject.login(self.config.mail_login, self.config.mail_pass)
        smtpobject.sendmail(self.config.mail_login, self.config.mail_login, self.message)         
        print("Successfully sent email")


def debug(text, data):
    print(text)
    print(type(data))
    print(data)

if __name__ == "__main__":
    CURR_DIR = os.path.dirname(os.path.realpath(__file__))
    CONF_FILE = CURR_DIR + '/config.yaml'
    config = Config(CONF_FILE)
    mycalls = ovhcalls(config)
    calls_sum = mycalls.summary()


    yes_calls = mycalls.get_yesterday_in(calls_sum)
    yes_sorted = mysort(yes_calls)
    yes_translated = translate(yes_sorted)

    sum_sorted = mysort(calls_sum)
    sum_translated = translate(sum_sorted)
    

    message_p2 = ""
    message_p1 = """From: script <""" + config.mail_login + """>
To: info <"""+ config.mail_login +""">
MIME-Version: 1.0
Content-type: text/html
Subject: resume des appels
Coucou,
petit resume des appels recus

<h1>appels entrants recus hier</h1>
"""
    for i in yes_translated:
        line = '<p> Numero: ', i[1]['calling'], ' ', i[1]['creationDatetime'], ' status: ', i[1]['wayType'],'</p>'
        line = ''.join(line)
        print(line)
        message_p2 = message_p2 + line + '\n'

    message_p2 = message_p2 + "<h2>appels recus depuis le debut du mois</h2>\n"

    for i in sum_translated:
        line = '<p> Numero: ', i[1]['calling'], ' ', i[1]['creationDatetime'], ' status: ', i[1]['wayType'],'</p>'
        line = ''.join(line)
        message_p2 = message_p2 + line + '\n'

    message = message_p1 +  message_p2
    print ('message:\n', message)

    send_mail(config, message)
